from faker import Faker 
import json            # To create a json file                 
from random import randint      # For number of emails 

fake = Faker()

def input_data(number_of_emails,user_id): 
    # dictionary 
    email_data = {}
    for i in range(0, number_of_emails):
        email_data[fake.email()] = user_id
    return email_data
  

def generate_fake_data():     
    contact = {}
    contact['user_name'] = fake.name().strip().replace(" ", "_")
    contact['first_name'] = fake.first_name()
    contact['last_name'] = fake.last_name()
    number_of_emails = randint(1, 10)
    sample_emails = input_data(number_of_emails, contact['user_name'])
    contact['emails'] = [{'email':key,"user_id":value} for key,value in sample_emails.items()]
    return json.dumps(contact)
