from celery import Celery
from celery.schedules import crontab

from init_app import create_app
from celery_task import create_contact, delete_contact

def create_celery(app):
    celery = Celery(app.import_name,
                    backend=app.config['CELERY_RESULT_BACKEND'],
                    broker=app.config['CELERY_BROKER_URL'])
    celery.conf.update(app.config)
    TaskBase = celery.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)
    celery.Task = ContextTask
    return celery

flask_app = create_app()
celery = create_celery(flask_app)


@celery.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    # Calls Implement a task that creates a Contact with random data every 15 seconds.
    sender.add_periodic_task(15.0, create_contact, name='Create Contact every 15 seconds')

    # Calls Implement a task that removes Contacts older than 1 minute.
    sender.add_periodic_task(60.0, delete_contact, name='Delete contact every 60 econds')