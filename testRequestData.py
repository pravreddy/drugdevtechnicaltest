import json

get = {'url': '/api/contact'}


# Good post request
successful_post = dict(url='/api/contact', data=json.dumps(
    dict(user_name="nico_femi", first_name="nico", last_name="femi")))

# Missing required field
unsuccessful_post = dict(url='/api/contact', data=json.dumps(
    dict(user_name="nico_femi", first_name="nico", last_namess="femi")))


update = dict(url='/api/contact/nico_femi', data=json.dumps({"first_name": "scot"}))

bad_update = dict(url='/api/contact/nico_femi', data=json.dumps({"first_namess": "-1233"}))

delete = dict(url='/api/contact/nico_femi')

search_user_name_like = dict(url='/api/contact?q={"filters":[{"name":"last_name", "op": "like", "val": "%femi%"}]}')

search_first_name_equals = dict(url='/api/contact?q={"filters":[{"name":"first_name", "op": "eq", "val": "nico"}]}')
