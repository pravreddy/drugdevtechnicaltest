import os

BASEDIR = os.path.abspath(os.path.dirname(__file__))
# Can be changed to use other DBs. Ex: 'postgresql://user:password@host:5432/mydatabase'
SQLITE_DB = 'sqlite:///' + os.path.join(BASEDIR, 'contacts.db')


class Config(object):
    DEBUG = False
    SECRET_KEY = 'this_is_a_secret_key'
    RELOAD = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL', SQLITE_DB)
    # To remove warnings
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    
    # Celery Config
    REDIS_HOST = "localhost"
    REDIS_PORT = 6379
    CELERY_BROKER_URL = os.environ.get('REDIS_URL', "redis://{host}:{port}/0".format(
        host=REDIS_HOST, port=str(REDIS_PORT)))
    CELERY_RESULT_BACKEND = CELERY_BROKER_URL
    CELERY_ACCEPT_CONTENT = ['application/json']
    CELERY_TASK_SERIALIZER = 'json'
    CELERY_RESULT_SERIALIZER = 'json'
    CELERY_TIMEZONE = 'Europe/London'
    CELERY_TEST_URL = 'http://localhost:5000/api/contact'


class DevelopmentConfig(Config):
    DEBUG = True

class ProductionConfig(Config):
    pass

config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig
}



