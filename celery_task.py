import json
import requests
from datetime import datetime, timedelta
from init_app import celery
from init_app import create_app

from random_json_generator.generator import generate_fake_data

flask_app = create_app()

url = flask_app.config['CELERY_TEST_URL']


@celery.task
def create_contact():
    """Create random data"""
    body = generate_fake_data()
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    """POST data"""
    r = requests.post(url, data=body, headers=headers)
    print(r.status_code)



@celery.task
def delete_contact():
    """Read all Data"""
    oneminuteless = datetime.now() - timedelta(minutes=1)
    contact = {}
    contact['filters'] =  [{'name':'date_created','op':'lt', 'val':str(oneminuteless)}]
    search_url = url+"?q="+json.dumps(contact)
    response = requests.get(search_url)
    customers = response.json()
    for customer in customers['objects']:
        delete_url = url+'/'+customer['user_name']
        r = requests.delete(delete_url)
        print(r.status_code)



