HOST: http://localhost:5000/

# contats

Restful API to view, manage and filter.

## contact by user_name [/api/contact/Jason_Smith]

### Get Contact [GET]

Get contact by user_name.

+ Request (application/json)

+ Response 200 (application/json)

    + Body
            {
            "date_created": "2019-09-22T23:10:26.223254", 
            "emails": [
                {
                "email": "jason.smith@mail.com", 
                "id": 3, 
                "user_id": "Jason_Smith"
                }
            ], 
            "first_name": "Json", 
            "last_name": "Smith", 
            "user_name": "Jason_Smith"
            }

### Delete Contact [DELETE]

Delete Contact by user_name.

+ Request (application/json)

+ Response 204 (application/json)


## Contacts collection [/api/contact]

### Get Contacts [GET]

Returns a list of all Contacts in the database.
Can be filtered by any field, by using the query parameter.

+ Parameters

    + q: {"filters":[{"name":"user_name", "op": "like", "val": "%john%"}]} (application/json) - (Optional)
    Query for filtering results. 
    Operators: eq (equals), like.
        
    
+ Response 200 (application/json)

            {
            "num_results": 1, 
            "objects": [
                {
                "date_created": "2019-09-22T23:10:26.223249", 
                "emails": [
                    {
                    "email": "john.doe@mail.com", 
                    "id": 1, 
                    "user_id": "John_Doe"
                    }, 
                    {
                    "email": "johndoe123@mail.com", 
                    "id": 2, 
                    "user_id": "John_Doe"
                    }, 
                    {
                    "email": "john.doe@me.com", 
                    "id": 4, 
                    "user_id": "John_Doe"
                    }
                ], 
                "first_name": "John", 
                "last_name": "Doe", 
                "user_name": "John_Doe"
                }
            ], 
            "page": 1, 
            "total_pages": 1
            }

### Create Contact [POST]

Create a new Contact.

+ Request (application/json)

        {
            "emails": [
                {
                "email": "john1.doe1@mail.com", 
                "user_id": "John1_Doe1"
                }, 
                {
                "email": "john1.doe1@me.com", 
                "user_id": "John1_Doe1"
                }
            ], 
            "first_name": "John1", 
            "last_name": "Doe1", 
            "user_name": "John1_Doe1"
        }

+ Response 201 (application/json)


    + Body

            {
                "date_created": "2019-09-22T23:18:24.657733",
                "emails": [
                    {
                        "email": "john1.doe1@mail.com",
                        "id": 8,
                        "user_id": "John1_Doe1"
                    },
                    {
                        "email": "john1.doe1@me.com",
                        "id": 9,
                        "user_id": "John1_Doe1"
                    }
                ],
                "first_name": "John1",
                "last_name": "Doe1",
                "user_name": "John1_Doe1"
            }
            
### Update Contact [PATCH]

Update Contact. The only field not available for update is user_name.

+ Request (application/json)

        {
            "emails": [
                {
                    "email": "john1.doe1@mail123.com", 
                    "user_id": "John1_Doe1"
                }, 
                {
                    "email": "john1.doe1@me123.com", 
                    "user_id": "John1_Doe1"
                }
            ]
        }

+ Response 200 (application/json)


    + Body

            {
                "date_created": "2019-09-22T23:18:24.657733",
                "emails": [
                    {
                        "email": "john1.doe1@mail123.com",
                        "id": 10,
                        "user_id": "John1_Doe1"
                    },
                    {
                        "email": "john1.doe1@me123.com",
                        "id": 11,
                        "user_id": "John1_Doe1"
                    }
                ],
                "first_name": "John1",
                "last_name": "Doe1",
                "user_name": "John1_Doe1"
            }
            