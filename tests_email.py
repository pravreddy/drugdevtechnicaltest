from app import app
import unittest
import json
from testRequestDataEmail import get_email, successful_post_with_email, update_with_email, delete_with_email


class ContactEmailTestCase(unittest.TestCase):
    client = app.test_client()

    # Checking that the api main endpoint is working
    def test_a_get(self):
        response = self.client.get(get_email['url'], content_type='application/json')
        self.assertEqual(response.status_code, 200)
    
    def test_b_post_new_contact_with_email(self):
        response = self.client.post(
            '/api/contact',
            data=successful_post_with_email['data'],
            content_type='application/json',
            follow_redirects=True
        )
        self.assertEqual(response.status_code, 201)

    def test_c_get_by_id_with_email(self):
        response = self.client.get(get_email['url']+'/nico_femi', content_type='application/json')
        self.assertIsNotNone(response.data)

    def test_d_post_patch_new_contact_with_email(self):
        response = self.client.patch(
            update_with_email['url'],
            data=update_with_email['data'],
            content_type='application/json',
            follow_redirects=True
        )
        self.assertEqual(response.status_code, 200)

    def test_e_delete_with_email(self):
        response = self.client.delete(
            delete_with_email['url'],
            content_type='application/json',
            follow_redirects=True
        )
        self.assertEqual(response.status_code, 204)


if __name__ == '__main__':
    unittest.main()
