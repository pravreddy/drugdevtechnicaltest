from flask import Flask
from models import db, Contact, Email
import csv
from app import app

app.debug = True
app.app_context().push()

with app.app_context():
    db.create_all()

    # Opening the csv file
    with open('data/CONTACT_MANAGEMENT_DETAILS.csv') as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=',')

        # Skipping the headers
        # csv_reader.next() -> python 2.X
        next(csv_reader)

        # Reading the table, line by line and adding to transaction
        for row in csv_reader:
            contact = Contact()

            contact.user_name = row[0]
            contact.first_name = row[1]
            if len(row) == 3:
                contact.last_name = row[2].strip()

            db.session.add(contact)

        # Transaction commit
        db.session.commit()

    # Opening the Email csv file
    with open('data/EMAIL_CONTACT_DETAILS.csv') as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=',')

        # Skipping the headers
        # csv_reader.next() -> python 2.X
        next(csv_reader)

        # Reading the table, line by line and adding to transaction
        for row in csv_reader:
            email = Email()

            email.id = row[0]
            email.email = row[1]
            if len(row) == 3:
                email.user_id = row[2].strip()

            db.session.add(email)

        # Transaction commit
        db.session.commit()
