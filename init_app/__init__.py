import os

from flask import Flask, request
from flask_restless import APIManager
from models import db, Contact
from config import config
from celery import Celery

celery = Celery()

def create_app(config_name=None):
    if config_name is None:
        config_name = os.environ.get('CONFIG', 'development')
    # Setting up the app, using configurations from the specified python file.
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    celery.config_from_object(app.config)

    # Initializing the database ORM
    db.init_app(app)

    return app