from flask_sqlalchemy import SQLAlchemy

from datetime import datetime

db = SQLAlchemy()

class Contact(db.Model):
    """
    Object-relational-map for table Contact.
    For simplicity, secondary tables will not be created for regions and addresses.
    Everything will be considered part of a single table, like in OLAP model.
    """

    user_name = db.Column('user_name', db.String(256), primary_key=True)
    first_name = db.Column('first_name', db.String(256), nullable=False)
    last_name = db.Column('last_name', db.String(256), nullable=False)
    date_created = db.Column(db.DateTime, default=datetime.now)
    emails = db.relationship('Email', backref='owner', lazy='dynamic', cascade="all, delete-orphan")

class Email(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	email = db.Column(db.String(128), nullable=False)
	user_id = db.Column(db.Integer, db.ForeignKey('contact.user_name'))

