import json

get_email = {'url': '/api/contact'}

sample_emails = {'nico.femi@myme.com': "nico_femi",  'nico.femi@me.com': "nico_femi", 'nico.femi@mail.com': "nico_femi"}

sample_emails_update = {'nico.femi@me.com': "nico_femi",  'nico.femi123@myme.com': "nico_femi", 'nico.femi@mymail.com': "nico_femi"}

d = {"user_name":"nico_femi","first_name":"nico", "last_name":"femi",
     "emails":[{'email':key,"user_id":value} for key,value in sample_emails.items()]}

updated_email = { "emails":[{'email':key,"user_id":value} for key,value in sample_emails_update.items()]}

# Good post request with email ids
successful_post_with_email = dict(url='/api/contact', data=json.dumps(d))

update_with_email = dict(url='/api/contact/nico_femi', data=json.dumps(updated_email))

delete_with_email = dict(url='/api/contact/nico_femi')
