# DrugDevTechnicalTest

Drug Dev Technical Tests is a python restful API made to get, post, update and search contacts in an SQL database and can serve as barebone structure base to any restful API alike.

The project runs un a virtual envorinment and has a setup script to install its necessary packages.
The API documentation, thechnologies used and setup instrucions are listed below.

## This is Step 1 Task

Step 1: Create a small contact management API with the following functionality:

    • List all contacts
    • Find a contact by username
    • Create a new contact
    • Update a contact
    • Delete a contact


At a minimum, the Contact model consists of a username, first name and last name. You may add any other columns necessary to implement the required functionality.

## Added Step 2 Task

Step 2: As a second step, extend your application to support email addresses.

Introduce a new Email model and allow a Contact to have multiple email addresses. Adjust the endpoints above to return emails in their output and accept emails as input as you see fit.

## Added Step 3 Task

Step 3: Use Celery to add asynchronous functionality to your application.

      • Implement a task that creates a Contact with random data every 15 seconds.
      • Implement a task that removes Contacts older than 1 minute.

For convenience, use Redis as your Celery broker and assume it will be installed and running on the test machine.


# installation

## create virtualenv
```shell
python3 -m venv venv (or use  python -m venv venv)
. venv/bin/activate
pip install -r requirements.txt (or use pip install -r requirements-dev.txt)
```

## run flask app
```shell
env 'FLASK_APP=app.py' flask run
```

## start redis backend (using docker)
```shell
docker run -d --name redis -p 6379:6379 redis
```

## run celery worker
```
celery -A celery_worker:celery worker --loglevel=DEBUG
```

## run celery beat for periodic tasks
```
celery -A celery_worker:celery beat --loglevel=INFO
```

#### Documentation
For documentation, open the ***docs*** directory, located right at the root of the repository.

#### Frameworks, libs and database used

   * [Flask] - A lightweight barebones framework for building python APIs.
  Chosen for being fast and using only the resources needed by the API.
  *Besides the SQLite, all other components used for this project are small addons to this framework, wicht is very extensible*.

  * [SQLite] - Embeded lightweight SQL database.
  Chosen for its ease of use and portability for this project. It is based on a single file, natively compatible with all versions of python and the database can be posted on the project repository and be ready for testing. Also, the DB engine can be easily changed in a single line of configuration, without having to change any code, thanks to the ORM engine used.

  * [Flask-SQLAlchemy] - SQL Object-Relational Mapping framework.
  Makes ORM simple and manageable, while it can also manage all the work if we need to change the database used, by just having to change a connection string. It's stable, powerful and compatible with SQLite, Postgresql, MySQL, Oracle, MS-SQL, Firebird, Sybase and others.

  * [Flask-Restless] - Python framework for creating simple restful APIs.
  Chosen for being just enough to build the requested API in a reliable and simple way, while even providing additional functionality, like pagination and a more advanced search mechanism.


#### Testing
For this API, there are 9 unit tests to show how to handle testing within the API structure.

To Run the tests, install the envoronment (explained below) activate the envoronment and run tests.py or Just use shell script `run_tests.sh`.

```sh
$ source venv/bin/activate
$ python tests.py
$ python tests_email.py
```

Some tests may be meant to err and will generate error messages. Its ok, as long as the assert methods get the desired values and don't fail.

# Setup
To run the project it's necessary to run the setup.sh script at least once, it will create the environment and install the necessary packages, given the pre-requisites below are met.

> The environment setup script is meant to run on a linux system. For windows users, a python2.7 or any python3.x virtual environment must be created first, activated and then the packages can be installed through pip using the requirements.txt file.

#### Pre-requisites

  For the setup script and API to run, the following software must be present.

    - **Python3.6** (originally built to run on python3, but totally compatible with 2.7.)
    - **pip**
    - **virtualenv**

#### Setup and how to run
  - Run setup.sh: installs the environment.
  - Run app.sh: Activates the environment and run the app.py.
  - Or use `env 'FLASK_APP=app.py' flask run` to start the app
That's it! The API will be running on the port 5000 of the localhost. ex.: localhost:5000/api/contact

#### Database Import
The database was imported using *importer.py*.
The CSV file containing the data is inside the data folder.

To reimport the data:
```sh
$ rm contact.db
$ source venv/bin/activate
$ python importer.py
$ deactivate
```

or Just run `./reimport_db.sh` .

#### External Links

   [Flask]: <http://flask.pocoo.org/>
   [SQLite]: <https://www.sqlite.org/index.html>
   [Flask-SQLAlchemy]: <http://flask-sqlalchemy.pocoo.org/2.3/>
   [Flask-Restless]: <https://flask-restless.readthedocs.io/en/stable/>

